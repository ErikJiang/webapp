FROM python:3.7-alpine

ENV PIP_URL=https://mirrors.aliyun.com/pypi/simple

COPY . /app
WORKDIR /app

RUN echo "- apk install packages ..." && \
    sed -i 's/dl-cdn.alpinelinux.org/mirrors.aliyun.com/g' /etc/apk/repositories && \
    apk --update add gcc musl-dev libffi-dev make tzdata openssl-dev linux-headers openssh-client && \
    cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime && \
    echo "- pip install packages ..." && \
    pip3 install --upgrade pip -i ${PIP_URL} && \
    pip3 install pipenv gunicorn --no-cache-dir -i ${PIP_URL} && \
    pipenv install --system --deploy --pypi-mirror ${PIP_URL}} && \
    echo "- removing package list ..." && \
    apk del libffi-dev gcc make linux-headers openssl-dev musl-dev && \
    rm -rf /var/cache/apk/*

ENTRYPOINT ["gunicorn", "-b", ":5000", "--access-logfile", "-", "--error-logfile", "-", "app:app", "--timeout", "90"]

EXPOSE 5000
