PROJECTNAME=$(shell basename "$(PWD)")
PROJECTNAME=webapp
GIT_BRANCH=`git rev-parse --abbrev-ref HEAD`
REGISTRY_ENDPOINT=registry.cn-hangzhou.aliyuncs.com/wise2c-dev
IMAGENAME=$(REGISTRY_ENDPOINT)/$(PROJECTNAME):$(GIT_BRANCH)

all: help

.PHONY: help run image

## run		run ci_trigger service.
run:
	@flask run

## image		build docker image.
image:
	@docker build -f Dockerfile -t $(IMAGENAME) ./
	@docker push $(IMAGENAME)

## help		print this help message and exit.
help: Makefile
	@echo "Choose a command run in "$(PROJECTNAME)":"
	@echo ""
	@echo "Usage: make [target]"
	@echo ""
	@echo "Valid target values are:"
	@echo ""
	@sed -n 's/^## //p' $<
